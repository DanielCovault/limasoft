--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

-- Started on 2019-02-13 18:14:07

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2164 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 563 (class 1247 OID 17500)
-- Name: domain_nota; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.domain_nota AS integer
	CONSTRAINT domain_nota_check CHECK (((VALUE >= 0) AND (VALUE <= 10)));


ALTER DOMAIN public.domain_nota OWNER TO postgres;

--
-- TOC entry 482 (class 1247 OID 17446)
-- Name: estado_domain; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.estado_domain AS character varying(8)
	CONSTRAINT estado_domain_check CHECK ((((VALUE)::text = 'activo'::text) OR ((VALUE)::text = 'inactivo'::text)));


ALTER DOMAIN public.estado_domain OWNER TO postgres;

--
-- TOC entry 570 (class 1247 OID 17531)
-- Name: id; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.id AS integer
	CONSTRAINT id_check CHECK (((VALUE >= 1) AND (VALUE <= '9999999999'::bigint)));


ALTER DOMAIN public.id OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 187 (class 1259 OID 17533)
-- Name: alumno; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.alumno (
    al_dni public.id NOT NULL,
    al_nombre character varying(50) NOT NULL,
    al_apellido character varying(50) NOT NULL,
    al_direccion character varying(50) NOT NULL,
    al_estado public.estado_domain DEFAULT 'activo'::character varying NOT NULL
);


ALTER TABLE public.alumno OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 17519)
-- Name: curso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.curso (
    crs_clave integer NOT NULL,
    crs_nombre character varying(50) NOT NULL,
    crs_estado public.estado_domain DEFAULT 'activo'::character varying NOT NULL
);


ALTER TABLE public.curso OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 17517)
-- Name: curso_crs_clave_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.curso_crs_clave_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.curso_crs_clave_seq OWNER TO postgres;

--
-- TOC entry 2165 (class 0 OID 0)
-- Dependencies: 185
-- Name: curso_crs_clave_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.curso_crs_clave_seq OWNED BY public.curso.crs_clave;


--
-- TOC entry 189 (class 1259 OID 17652)
-- Name: detallexcurso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detallexcurso (
    dt_clave integer NOT NULL,
    dt_al_clave integer NOT NULL,
    dt_crs_nombre character varying(50) NOT NULL,
    dt_nota public.domain_nota NOT NULL,
    dt_estado public.estado_domain DEFAULT 'activo'::character varying NOT NULL
);


ALTER TABLE public.detallexcurso OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 17650)
-- Name: detallexcurso_dt_clave_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detallexcurso_dt_clave_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detallexcurso_dt_clave_seq OWNER TO postgres;

--
-- TOC entry 2166 (class 0 OID 0)
-- Dependencies: 188
-- Name: detallexcurso_dt_clave_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.detallexcurso_dt_clave_seq OWNED BY public.detallexcurso.dt_clave;


--
-- TOC entry 2020 (class 2604 OID 17522)
-- Name: curso crs_clave; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.curso ALTER COLUMN crs_clave SET DEFAULT nextval('public.curso_crs_clave_seq'::regclass);


--
-- TOC entry 2023 (class 2604 OID 17655)
-- Name: detallexcurso dt_clave; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detallexcurso ALTER COLUMN dt_clave SET DEFAULT nextval('public.detallexcurso_dt_clave_seq'::regclass);


--
-- TOC entry 2154 (class 0 OID 17533)
-- Dependencies: 187
-- Data for Name: alumno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.alumno (al_dni, al_nombre, al_apellido, al_direccion, al_estado) FROM stdin;
1	daniel	covault	el valle	activo
6	Jose	covault	el valle	activo
5	toledo	covault	el valle	activo
2	david	covault	porlamar	inactivo
7	ramon	covault	el valle	activo
10	alfredo	covault	Pampatar	activo
4	jendric	covault	los robles	activo
3	manuel	covault	porlamar	activo
\.


--
-- TOC entry 2153 (class 0 OID 17519)
-- Dependencies: 186
-- Data for Name: curso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.curso (crs_clave, crs_nombre, crs_estado) FROM stdin;
1	MATEMATICAS	activo
3	INGLES	activo
2	DATOS	activo
\.


--
-- TOC entry 2167 (class 0 OID 0)
-- Dependencies: 185
-- Name: curso_crs_clave_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.curso_crs_clave_seq', 3, true);


--
-- TOC entry 2156 (class 0 OID 17652)
-- Dependencies: 189
-- Data for Name: detallexcurso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detallexcurso (dt_clave, dt_al_clave, dt_crs_nombre, dt_nota, dt_estado) FROM stdin;
1	1	INGLES	10	activo
2	2	INGLES	10	activo
3	2	DATOS	5	activo
4	7	DATOS	10	activo
5	7	MATEMATICAS	3	activo
\.


--
-- TOC entry 2168 (class 0 OID 0)
-- Dependencies: 188
-- Name: detallexcurso_dt_clave_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detallexcurso_dt_clave_seq', 5, true);


--
-- TOC entry 2026 (class 2606 OID 17530)
-- Name: curso curso_crs_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.curso
    ADD CONSTRAINT curso_crs_nombre_key UNIQUE (crs_nombre);


--
-- TOC entry 2030 (class 2606 OID 17541)
-- Name: alumno pk_alumno; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alumno
    ADD CONSTRAINT pk_alumno PRIMARY KEY (al_dni);


--
-- TOC entry 2028 (class 2606 OID 17528)
-- Name: curso pk_curso; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.curso
    ADD CONSTRAINT pk_curso PRIMARY KEY (crs_clave);


--
-- TOC entry 2032 (class 2606 OID 17661)
-- Name: detallexcurso pk_detallexcusro; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detallexcurso
    ADD CONSTRAINT pk_detallexcusro PRIMARY KEY (dt_clave, dt_al_clave, dt_crs_nombre);


--
-- TOC entry 2033 (class 2606 OID 17662)
-- Name: detallexcurso fk_dt_al_clave; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detallexcurso
    ADD CONSTRAINT fk_dt_al_clave FOREIGN KEY (dt_al_clave) REFERENCES public.alumno(al_dni) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2034 (class 2606 OID 17667)
-- Name: detallexcurso fk_dt_crs_clave; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detallexcurso
    ADD CONSTRAINT fk_dt_crs_clave FOREIGN KEY (dt_crs_nombre) REFERENCES public.curso(crs_nombre) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2019-02-13 18:14:07

--
-- PostgreSQL database dump complete
--

