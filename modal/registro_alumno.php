
	<!-- Modal -->
	<div class="modal fade" id="nuevoAlumno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"><i class='glyphicon glyphicon-edit'></i> Agregar nuevo Alumno</h4>
		  </div>
		  <div class="modal-body">
			<form class="form-horizontal" method="post" id="guardar_alumno" name="guardar_alumno">
			<div id="resultados_ajax"></div>
			<div class="form-group">
				<label for="al_dni" class="col-sm-3 control-label">* DNI</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="al_dni" name="al_dni" required>
				</div>
			  </div>
			  <div class="form-group">
				<label for="al_nombre" class="col-sm-3 control-label">* Nombre</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="al_nombre" name="al_nombre" required>
				</div>
			  </div>

				<div class="form-group">
				<label for="al_apellido" class="col-sm-3 control-label">* Apellido</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="al_apellido" name="al_apellido" required>
				</div>
			  </div>
			  			  
			  <div class="form-group">
				<label for="al_direccion" class="col-sm-3 control-label"> Dirección</label>
				<div class="col-sm-8">
					<textarea class="form-control" id="al_direccion" name="al_direccion"   maxlength="50" ></textarea>  
				</div>
			  </div>

		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="guardar_datos">Guardar datos</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>
