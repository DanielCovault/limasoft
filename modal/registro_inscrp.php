
	<!-- Modal -->
	<div class="modal fade" id="nuevoInscrp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"><i class='glyphicon glyphicon-edit'></i> Asignar Curso por Alumno</h4>
		  </div>
		  <div class="modal-body">
			<form class="form-horizontal" method="post" id="guardar_curso" name="guardar_curso">
			<div id="resultados_ajax"></div>


		  <?php
		    { 
					include ("conexion/conexion.php");//Contiene funcion que conecta a la base de datos

		    //echo "Conexion exitosa";
		    $query="SELECT * FROM alumno;";
		    $result=pg_query($query) or die('Query failed: ' . pg_last_error());
		    $rows = pg_numrows($result);
		    ?>
			<div class="form-group">
				<label for="al_dni" class="col-sm-3 control-label">* DNI Alumno</label>
		    	<div class="col-sm-8">
		      	<select class="form-control" id="al_dni" name="al_dni" selected="" required>
		    
		      <?php for($i=1;$i<=$rows; $i++){$line = pg_fetch_array($result, null, PGSQL_ASSOC);
		      echo "<option value='".$line['al_dni']."'>".$line['al_dni']."</option>";
		      }//echo "</select>";
		  pg_close();}?> 
		      </select>
			  	</div>
				</div>
				

		  <?php
		    { 
					include ("conexion/conexion.php");//Contiene funcion que conecta a la base de datos

		    //echo "Conexion exitosa";
		    $query="SELECT * FROM curso;";
		    $result=pg_query($query) or die('Query failed: ' . pg_last_error());
		    $rows = pg_numrows($result);
		    ?>
			<div class="form-group">
				<label for="crs_nombre" class="col-sm-3 control-label">* Curso</label>
		    	<div class="col-sm-8">
		      	<select class="form-control" id="crs_nombre" name="crs_nombre" selected="" required>
		    
		      <?php for($i=1;$i<=$rows; $i++){$line = pg_fetch_array($result, null, PGSQL_ASSOC);
		      echo "<option value='".$line['crs_nombre']."'>".$line['crs_nombre']."</option>";
		      }//echo "</select>";
		  pg_close();}?> 
		      </select>
			  	</div>
			  </div>

			  <div class="form-group">
				<label for="nota" class="col-sm-3 control-label">* Nota</label>
				<div class="col-sm-8">
				  <input type="number" class="form-control" id="nota" name="nota" min="0"  max="10" required>
				</div>
			  </div>

		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="guardar_datos">Guardar datos</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>

		
