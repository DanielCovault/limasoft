
	<!-- Modal -->
	<div class="modal fade" id="nuevoInscrp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"><i class='glyphicon glyphicon-edit'></i> Agregar nuevo Curso</h4>
		  </div>
		  <div class="modal-body">
			<form class="form-horizontal" method="post" id="guardar_curso" name="guardar_curso">
			<div id="resultados_ajax"></div>

					<div class="form-group">
              <label for="nombre_cliente" class="col-sm-1 col-md-1 control-label">Alumno</label>
				  <div class="col-sm-3 col-md-3">
				  <input type="text" class="form-control input-sm" id="al_nombre" name="al_nombre" placeholder="Nombre o DNI del alumno" required>
				  </div>
			  </div>
        <div class="form-group">
            <label for="nombre_cliente" class="col-sm-1 col-md-1 control-label">Cliente</label>
					<div class="col-sm-3 col-md-3">
     			<input type="text" class="form-control input-sm" id="al_dni" name="al_dni" placeholder="ID del cliente" required readonly>
			</div>
			</div>

			  <div class="form-group">
				<label for="crs_nombre" class="col-sm-3 control-label">* Nombre</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="crs_nombre" name="crs_nombre" required>
				</div>
			  </div>

		  <?php
		    { 
					include ("conexion/conexion.php");//Contiene funcion que conecta a la base de datos

		    //echo "Conexion exitosa";
		    $query="SELECT * FROM curso;";
		    $result=pg_query($query) or die('Query failed: ' . pg_last_error());
		    $rows = pg_numrows($result);
		    ?>
			<div class="form-group">
				<label for="crs_nombre" class="col-sm-3 control-label">Curso</label>
		    	<div class="col-sm-8">
		      	<select class="form-control" id="crs_nombre" name="crs_nombre" selected="" required>
		    
		      <?php for($i=1;$i<=$rows; $i++){$line = pg_fetch_array($result, null, PGSQL_ASSOC);
		      echo "<option value='".$line['crs_nombre']."'>".$line['crs_nombre']."</option>";
		      }//echo "</select>";
		  pg_close();}?> 
		      </select>
			  	</div>
			  </div>

		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="guardar_datos">Guardar datos</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>
	
	<script type="text/javascript" src="js/inscripciones.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script>
		$(function() {
						$("#al_nombre").autocomplete({
							source: "./ajax/autocomplete/clientes.php",
							minLength: 2,
							select: function(event, ui) {
								event.preventDefault();
								$('#al_dni').val(ui.item.al_dni);
								$('#al_nombre').val(ui.item.al_nombre);
							 }
						})				
					});
					
	$("#al_nombre" ).on( "keydown", function( event ) {
						if (event.keyCode== $.ui.keyCode.LEFT || event.keyCode== $.ui.keyCode.RIGHT || event.keyCode== $.ui.keyCode.UP || event.keyCode== $.ui.keyCode.DOWN || event.keyCode== $.ui.keyCode.DELETE || event.keyCode== $.ui.keyCode.BACKSPACE )
						{
							$("#al_dni" ).val("");
							$("#mail" ).val("");
											
						}
						if (event.keyCode==$.ui.keyCode.DELETE){
							$("#al_nombre" ).val("");
							$("#al_dni" ).val("");
						}
			});	
	</script>

		
