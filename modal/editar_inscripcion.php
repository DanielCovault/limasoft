	<!-- Modal -->
	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"><i class='glyphicon glyphicon-edit'></i> Editar Inscripción</h4>
		  </div>
		  <div class="modal-body">
			<form class="form-horizontal" method="post" id="editar_inscripcion" name="editar_inscripcion">
			<div id="resultados_ajax2"></div>
			  <div class="form-group">
				<label for="mod_al_dni" class="col-sm-3 control-label">* DNI</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="mod_al_dni" name="mod_al_dni" disabled>
					<input type="hidden" name="mod_id" id="mod_id">
				</div>
			  </div>

			  <div class="form-group">
				<label for="mod_crs_nombre" class="col-sm-3 control-label">Apellido</label>
				<div class="col-sm-8">
					<textarea class="form-control" id="mod_crs_nombre" name="mod_crs_nombre"   maxlength="50" ></textarea>  
				</div>
			  </div>

			  <div class="form-group">
				<label for="mod_nota" class="col-sm-3 control-label">* Nota</label>
				<div class="col-sm-8">
					<input type="text" class="form-control"  id="mod_nota" name="mod_nota"   maxlength="255" >
				</div>
			  </div>

			  <div class="form-group">
				<label for="mod_estado" class="col-sm-3 control-label">Estado</label>
				<div class="col-sm-8">
				 <select class="form-control" id="mod_estado" name="mod_estado" required>
					<option value="">-- Selecciona estado --</option>
					<option value="activo" selected>Activo</option>
					<option value="inactivo">Inactivo</option>
				  </select>
				</div>
			  </div>			  
			  
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="actualizar_datos">Actualizar datos</button>
			</div>
		  </form>
		</div>
	  </div>
	</div>
