$(document).ready(function(){
    load(1);
});

function load(page){
    var q= $("#q").val();
    $("#loader").fadeIn('slow');
    $.ajax({
        url:'./ajax/buscar_inscripciones.php?action=ajax&page='+page+'&q='+q,
         beforeSend: function(objeto){
         $('#loader').html('<img src="./img/ajax-loader.gif"> Cargando...');
      },
        success:function(data){
            $(".outer_div").html(data).fadeIn('slow');
            $('#loader').html('');
            
        }
    })
}



    function eliminar (id)
{
    var q= $("#q").val();
if (confirm("Realmente deseas desactivar el Alumno")){	
$.ajax({
type: "GET",
url: "./ajax/buscar_inscripciones.php",
data: "id="+id,"q":q,
 beforeSend: function(objeto){
    $("#resultados").html("Mensaje: Cargando...");
  },
success: function(datos){
$("#resultados").html(datos);
load(1);
}
    });
}
}

$( "#guardar_curso" ).submit(function( event ) {
$('#guardar_datos').attr("disabled", true);

var parametros = $(this).serialize();
$.ajax({
    type: "POST",
    url: "ajax/nuevo_alumnoxcurso.php",
    data: parametros,
     beforeSend: function(objeto){
        $("#resultados_ajax").html("Mensaje: Cargando...");
      },
    success: function(datos){
    $("#resultados_ajax").html(datos);
    $('#guardar_datos').attr("disabled", false);
    load(1);
  }
});
event.preventDefault();
})

$( "#editar_inscripcion" ).submit(function( event ) {
$('#actualizar_datos').attr("disabled", true);

var parametros = $(this).serialize();
$.ajax({
    type: "POST",
    url: "ajax/editar_inscripcion.php",
    data: parametros,
     beforeSend: function(objeto){
        $("#resultados_ajax2").html("Mensaje: Cargando...");
      },
    success: function(datos){
    $("#resultados_ajax2").html(datos);
    $('#actualizar_datos').attr("disabled", false);
    load(1);
  }
});
event.preventDefault();
})

function obtener_datos(id){
    var al_dni = $("#al_clave"+id).val();
    var crs_nombre = $("#crs_nombre"+id).val();
    var nota = $("#nota"+id).val();

    $("#mod_al_dni").val(al_dni);
    $("#mod_crs_nombre").val(crs_nombre);
    $("#mod_nota").val(nota);
    $("#mod_id").val(id);		
}