<?php
	if (empty($_POST['mod_id'])) {
           $errors[] = "ID vacío";
        }else if (empty($_POST['mod_nombre'])){
           $errors[] = "Nombre vacío";
		}  else if ($_POST['mod_apellido']==""){
			$errors[] = "El campo Apellido del alumno esta vacio";
        }  else if ($_POST['mod_estado']==""){
			$errors[] = "Selecciona el estado del cliente";

		}  else if (
			!empty($_POST['mod_id']) &&
			!empty($_POST['mod_nombre']) &&
			!empty($_POST['mod_apellido']) &&
			$_POST['mod_estado']!="" 
		){
		/* Connect To Database*/ 
		require_once ("../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
		// escaping, additionally removing everything that could be (html/javascript-) code
		$nombre=pg_escape_string(strip_tags($_POST["mod_nombre"],ENT_QUOTES));
		$apellido=pg_escape_string(strip_tags($_POST["mod_apellido"],ENT_QUOTES));
		$direccion=pg_escape_string(strip_tags($_POST["mod_direccion"],ENT_QUOTES));
		$estado=pg_escape_string(strip_tags($_POST["mod_estado"],ENT_QUOTES));
		
		$id_alumno=intval($_POST['mod_id']);
		$sql="UPDATE alumno SET al_nombre='".$nombre."', al_apellido='".$apellido."', al_direccion='".$direccion."',  al_estado='".$estado."' WHERE al_dni='".$id_alumno."'";
		$query_update = pg_query($sql);
			if ($query_update){
				$messages[] = "Proveedor ha sido actualizado satisfactoriamente.";
			} else{
				$errors []= "Lo siento algo ha salido mal intenta nuevamente.";
			}
		} else {
			$errors []= "Error desconocido.";
		}
		
		if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
?>