<?php
	/* Connect To Database*/
	require_once ("../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
	if (isset($_GET['id'])){
		$id_proveedor=intval($_GET['id']);
		$estado="inactivo";
			if ($delete1=pg_query("UPDATE alumno SET al_estado='".$estado."' WHERE al_dni='".$id_proveedor."'")){
			?>
			<div class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Aviso!</strong> Alumno desactivado con exito.
			</div>
			<?php 
		}else {
			?>
			<div class="alert alert-danger alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Error!</strong> Lo siento algo ha salido mal intenta nuevamente.
			</div>
			<?php			
		}	
		
		
	}
	if($action == 'ajax'){
		// escaping, additionally removing everything that could be (html/javascript-) code
         $q = (strip_tags($_REQUEST['q'], ENT_QUOTES));
		 //$aColumns = array('nombre_empresa','nombre_responsable', 'rif' );//Columnas de busqueda
		 $sTable = "alumno";
		 $sWhere = "";
		/*if ( $_GET['q'] != "" )
		{
			//$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".$q."%' or fact_venta.nrofac::text like '%$q%') ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}*/
		if ( $_GET['q'] != "" )
		{
		$sWhere.= " WHERE  (al_nombre like '%$q%')  ";			
		}
		$sWhere.=" order by al_dni DESC";
		include 'pagination.php'; //include pagination file
		//pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = 10; //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = pg_query("SELECT FROM $sTable  $sWhere");
		$row= pg_fetch_array($count_query);
		$numrows =pg_num_rows($count_query);
		$total_pages = ($numrows/$per_page);
		$reload = './productos2.php';
		//main query to fetch the data
		$sql="SELECT * FROM  $sTable $sWhere LIMIT $per_page offset $offset";
		$query = pg_query($sql);
		//loop through fetched data
		if ($numrows>0){
			
			?>
			<div class="table-responsive">
			  <table class="table">
				<tr  class="info">
					<th class='text-center'>ID</th>
					<th class='text-center'>Nombre</th>
					<th class='text-center'>Apellido</th>
					<th class='text-center'>Dirección</th>
					<th class='text-center'>Estado</th>
					<th class='text-center'>Acción</th>		

				</tr>
				<?php
				while ($row=pg_fetch_array($query)){
						$al_clave=$row['al_dni'];
						$al_nombre=$row['al_nombre'];
						$al_apellido=$row['al_apellido'];
						$al_direccion=$row['al_direccion'];
						$estado=$row['al_estado'];

						if ($estado=='activo'){$text_estado="Activo";$label_class='label-success';}
						else{$text_estado="Desactivado";$label_class='label-danger';}
					?>
					<input type="hidden" value="<?php echo $al_nombre;?>" id="al_nombre<?php echo $al_clave;?>">
					<input type="hidden" value="<?php echo $al_apellido;?>" id="al_apellido<?php echo $al_clave;?>">
					<input type="hidden" value="<?php echo $al_direccion;?>" id="al_direccion<?php echo $al_clave;?>">
					<tr>
						<td class='text-center'><?php echo $al_clave; ?></td>
						<td class='text-center'><?php echo $al_nombre; ?></td>
						<td class='text-center'><?php echo $al_apellido; ?></td>
						<td class='text-center'><?php echo $al_direccion; ?></td>
						<td class='text-center'><span class="label <?php echo $label_class;?>"><?php echo $text_estado; ?></span></td>
				
					<td class='text-center'><span>
					<a href="#" class='btn btn-default' title='Editar Proveedor' onclick="obtener_datos('<?php echo $al_clave;?>');" data-toggle="modal" data-target="#myModal2"><i class="glyphicon glyphicon-edit"></i></a> 
					<a href="#" class='btn btn-default' title='Desactivar Proveedor' onclick="eliminar('<?php echo $al_clave; ?>')"><i class="glyphicon glyphicon-remove"></i> </a></span></td>
						
					</tr>
					<?php
				}
				?>
				<tr>
					<td colspan=7><span class="pull-right"><?php
					 echo paginate($reload, $page, $total_pages, $adjacents);
					?></span></td>
				</tr>
			  </table>
			</div>
			<?php
		}
	}
?>