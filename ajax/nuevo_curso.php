<?php
	if (empty($_POST['crs_nombre'])) {
           $errors[] = " Campo NOMBRE Vacio";
		} else if (!empty($_POST['crs_nombre'])){
		/* Connect To Database*/
		require_once ("../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
		// escaping, additionally removing everything that could be (html/javascript-) code
		$crs_nombre=pg_escape_string(strip_tags($_POST["crs_nombre"],ENT_QUOTES));
		//$estado=pg_escape_string(strip_tags($_POST["estado"],ENT_QUOTES));
		$query=pg_query("SELECT FROM curso WHERE crs_nombre= '$crs_nombre'");
		$count=pg_num_rows($query);
		if ($count!=0){
			$errors []= "Ya existe el Curso";
		}else {
			$sql="INSERT INTO curso (crs_nombre) VALUES ('$crs_nombre')";
			$query_new_insert = pg_query($sql);
			if ($query_new_insert){
				$messages[] = "Curso registrado satisfactoriamente.";
			} else{
				$errors []= "Lo siento algo ha salido mal intenta nuevamente.";
			}
			}
		} else {
			$errors []= "Error desconocido.";
		}
		
		if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
?>