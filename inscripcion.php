<!DOCTYPE html>
<html lang="es">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

  <head>
      <title>Inscripciones</title>
			<?php include('gcw_archivos/navbar.php'); ?>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/guaicaweb.css">

    <script src="js/jquery-3.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container">
	<div class="panel panel-primary">
		<div class="panel-heading">
		    <div class="btn-group pull-right">
				<button type='button' class="btn btn-DC" data-toggle="modal" data-target="#nuevoInscrp"><span class="glyphicon glyphicon-plus" ></span> Inscribir Alumno</button>
			</div>
			<h4><i class='glyphicon glyphicon-search'></i> Buscar cusros por Alumno</h4>
		</div>
		<div class="panel-body">
			<?php
				include("modal/registro_inscrp.php");
				include("modal/editar_inscripcion.php");
			?>
			<form class="form-horizontal" role="form" id="datos_cotizacion">				
						<div class="form-group row">
							<label for="q" class="col-md-2 control-label">Buscar notas del Alumno o Cursos inscritos.</label>
							<div class="col-md-5">
								<input type="text" class="form-control" id="q" placeholder="DNI del Alumno o Nombre del Curso" onkeyup='load(1);'>
							</div>
							<div class="col-md-3">
								<button type="button" class="btn btn-default" onclick='load(1);'>
									<span class="glyphicon glyphicon-search" ></span> Buscar</button>
								<span id="loader"></span>
							</div>							
						</div>				
			</form>
				<div id="resultados"></div><!-- Carga los datos ajax -->
				<div class='outer_div'></div><!-- Carga los datos ajax -->			
 		</div>
	</div>
	</div>
	<script type="text/javascript" src="js/inscripciones.js"></script>

	<!--script type="text/javascript" src="js/inscripciones.js"></script-->

 <script type="text/javascript">
  </body>
</html>